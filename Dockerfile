FROM node:latest

RUN mkdir /usr/src/api

WORKDIR /usr/src/api

COPY ./* ./

RUN npm install

EXPOSE 3000

CMD [ "npm", "start" ]