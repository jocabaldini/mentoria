const mongoose = require('mongoose')

const studentSchema = new mongoose.Schema({
  name: String,
  age: Number,
  school_grade: String,
  address: String,
  phone: String
})

module.exports = mongoose.model('Student', studentSchema)
