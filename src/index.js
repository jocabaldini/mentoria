'use strict'

const Hapi = require('@hapi/hapi')
const Student = require('./models/student')

const init = async () => {

  const server = Hapi.server({
    port: 3000,
    host: '127.0.0.1'
  })

  server.route({
    method: 'POST',
    path: '/student',
    handler: (request, h) => {
        
      let response
    
      try {
        const newStudent = request.payload
        console.log(newStudent)

        const student = new Student(newStudent)
        student.save();

        response = student.toObject()
      } catch (e) {
          console.log(e.message)
      }
            
      return response

      /*  
        const name = request.payload.name ? request.payload.name : 'stranger'
        const age = request.payload.age ? request.payload.age : 5
        const school_grade = request.payload.school_grade ? request.payload.school_grade : 'P1'
        const address = request.payload.address ? request.payload.address : 'Rua dos bobos, 0'
        const phone = request.payload.phone ? request.payload.phone : '(55) 55555-5555'

        return `Hello ${name}!`
      */
    }
  })

  server.route({
    method: 'GET',
    path: '/',
    handler: (request, h) => {
        return 'Hello World!!!'
    }
  })

  await server.start()
  console.log('Server running on %s', server.info.uri)
};

process.on('unhandledRejection', (err) => {
    console.log(err)
    process.exit(1)
});

init()